import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(private http: HttpClient) {}

  file: File;
  title = '';
  description ='';
  author='';

  onFileChange(event) {
    this.file = event.target.files[0];
    

    
  }
  uploadFile() {
    let formData = new FormData();
    if (this.file.name) {

      formData.append('picture',this.file, this.file.name);
      formData.append('title',this.title);
      formData.append('description',this.description);
      formData.append('author',this.author);
      this.http.post('http://localhost:3001/', formData).subscribe( () => {
          
      })
    }
      console.log(formData);

  }
}
